#include "colors.inc"
#include "textures.inc"
#include "shapes.inc"
#include "glass.inc"
#include "metals.inc"
#include "woods.inc"
#include "stones.inc"
#include "golds.inc"

camera {
	orthographic
	location <0, 3.38, -1.7>
	right x*image_width/image_height
	angle 50
	look_at <0, 0.5, 0>
}

light_source {
	<-5, 10, 0> color White
	parallel
	point_at <0,0,0>
}

light_source {
	<0, 0, -10> color rgb <0.1,0.1,0.1>
	parallel
	point_at <0,1.5,0>
}

plane {
	y, 0
	pigment {
		color Clear
	}
}

#declare stuff = texture {
	pigment {
		spherical
		turbulence <1, 1, 1>
		omega 1
		lambda 6

		color_map {
			[ 0.1 color rgb <0.478431, 0.513725, 0.462745> ]
		}
	}

	normal {
		boxed
		30
		turbulence <1, 1, 1>
		omega 1
		lambda 6
		frequency 15
	}

	finish {
		ambient rgb <0, 0, 0>
		diffuse 0.3
		specular 0.6
		conserve_energy

		reflection {
			rgb <0.1, 0.1, 0.1>
			metallic 1
		}
	}
	scale 1
}

#declare s=function(x,y){3/(x*x+y*y+.5)}
#declare g=function{pattern{granite scale 6-y*2}}
#declare f=function(x,y,z,a,b){pow(g(x*a+z*b,y+clock*3,x*b-z*a),.6)}
#declare h=function(x,y,z,a){f(x,y,z,sin(a),cos(a))}

object {
	difference {
		Round_Box(<-1, 0, -1>, <1, 1.0, 1>, 0.05, true)
		sphere {
			<0,1.5,0>, 1.0
			texture { stuff }
		}
	}
	texture { stuff }
}

object {
	difference {
		cylinder {
			<0,-0.08,0>, <0,0.08,0>, 0.9
		}
		cylinder {
			<0,-0.1,0>, <0,0.1,0>, 0.85
		}
	}
	rotate -(clock*180)*y
	texture { pigment { color Red } finish { F_MetalE } }

	translate <0,1.5,0>
}

object {
	difference {
		cylinder {
			<0,-0.08,0>, <0,0.08,0>, 0.8
		}
		cylinder {
			<0,-0.1,0>, <0,0.1,0>, 0.75
		}
	}
	rotate -(clock*180)*z
	texture { T_Copper_1E }
	translate <0,1.5,0>
}

object {
	difference {
		cylinder {
			<0,-0.08,0>, <0,0.08,0>, 0.7
		}
		cylinder {
			<0,-0.1,0>, <0,0.1,0>, 0.65
		}
	}
	rotate -(clock*180)*x
	texture { T_Gold_1E }
	translate <0,1.5,0>
}

box {
	<-0.2,-0.2,-0.2>, <0.2,0.2,0.2>
	texture { pigment { color White filter 1-clock } }
	translate <0,1.4,0>
}