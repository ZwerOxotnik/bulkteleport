local function energizer(job, name, energy, tiles)
	local s_corner = tiles/2 - 0.5

	data:extend({
		{
			type = "item",
			name = "bulkteleport-energizer-"..name,
			icon = "__bulkteleport__/graphics/"..name..".png",
			icon_size = 32,
			flags = {"hidden"},
			stack_size = 10,
		},
		{
			type = "furnace",
			name = "bulkteleport-energizer-"..name,
			icon = "__bulkteleport__/graphics/"..name..".png",
			icon_size = 32,
			flags = {
				"placeable-neutral",
				"player-creation",
				"not-deconstructable",
			},
	    mined_sound = nil,
	    minable = nil,
	    collision_mask = {},
			max_health = 300,
	    collision_box = nil,
			selection_box = {{-s_corner, -s_corner}, {0, 0}},
			module_specification = {
				module_slots = 0
			},
			corpse = "big-remnants",
			dying_explosion = "medium-explosion",
			allowed_effects = {
				"consumption",
				"speed",
				"productivity",
				"pollution",
			},
			crafting_categories = {
				"bulkteleport",
			},
			--fixed_recipe = "bulkteleport-"..job,
			crafting_speed = 1.0,
			energy_source = {
				type = "electric",
				usage_priority = "primary-input",
				emissions = 0.03 / 3.5
			},
			energy_usage = energy,
			--ingredient_count = 1,
			result_inventory_size = 1,
			source_inventory_size = 1,
			animation = {
				filename = "__bulkteleport__/graphics/"..name.."-frames.png",
				width = tiles*48,
				height = tiles*48,
				frame_count = 49,
				line_length = 7,
				shift = {0, 0},
				animation_speed = 1.0,
			},
			vehicle_impact_sound = { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
			repair_sound = { filename = "__base__/sound/manual-repair-simple.ogg" },
			open_sound = { filename = "__base__/sound/machine-open.ogg", volume = 0.85 },
			close_sound = { filename = "__base__/sound/machine-close.ogg", volume = 0.75 },
			order = "z",
			placeable_by = {
				item = "bulkteleport-buffer-"..name,
				count = 1,
			},
			tile_width = tiles,
			tile_height = tiles,
		},
	})
end

local function buffer(name, size, tiles)
	local corner = tiles/2 - 0.1
	local s_corner = tiles/2 - 0.5

	data:extend({
		{
			type = "item",
			name = "bulkteleport-buffer-"..name,
			icon = "__bulkteleport__/graphics/"..name..".png",
			icon_size = 32,
			flags = {"hidden"},
			stack_size = 10,
		},
		{
			type = "container",
			name = "bulkteleport-buffer-"..name,
			icon = "__bulkteleport__/graphics/"..name..".png",
			icon_size = 32,
			flags = {"placeable-neutral", "player-creation"},
			minable = {mining_time = 1, result = "bulkteleport-"..name},
			max_health = 300,
			corpse = "small-remnants",
			open_sound = { filename = "__base__/sound/metallic-chest-open.ogg", volume = 0.65 },
			close_sound = { filename = "__base__/sound/metallic-chest-close.ogg", volume = 0.7 },
			resistances =
			{
				{
					type = "fire",
					percent = 90
				}
			},
    	collision_box = {{-corner, -corner}, {corner, corner}},
			selection_box = {{0, 0}, {s_corner, s_corner}},
			inventory_size = size,
			vehicle_impact_sound = { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
			picture =
			{
				filename = "__bulkteleport__/graphics/"..name.."-container.png",
				priority = "extra-high",
				width = tiles*48,
				height = tiles*48,
			},
			circuit_wire_connection_point = circuit_connector_definitions["chest"].points,
			circuit_connector_sprites = circuit_connector_definitions["chest"].sprites,
			circuit_wire_max_distance = default_circuit_wire_max_distance,
			placeable_by = {
				item = "bulkteleport-"..name,
				count = 1,
			},
			tile_width = tiles,
			tile_height = tiles,
		},
	})
end

local function dummy(name, tiles)
	local corner = tiles/2 - 0.1

	data:extend({
		{
			type = "simple-entity-with-force",
			name = "bulkteleport-"..name,
			flags = {
				"player-creation",
			},
			selectable_in_game = true,
			build_sound = nil,
			mined_sound = nil,
			created_smoke = nil,
			minable = {
				mining_time = 1,
				result = placeable,
			},
			collision_mask = {
				"object-layer",
			},
			collision_box = {{-corner, -corner}, {corner, corner}},
			selection_box = {{-corner, -corner}, {corner, corner}},
			picture = {
				filename = "__bulkteleport__/graphics/"..name.."-frames.png",
				width = tiles*48,
				height = tiles*48,
			},
			render_layer = "object",
			tile_width = tiles,
			tile_height = tiles,
		},
	})
end

local function teleporter(tier, energy, size, tiles)
	energizer("job1", "send"..tier, energy, tiles)
	energizer("job1", "recv"..tier, energy, tiles)
	buffer("send"..tier, size, tiles)
	buffer("recv"..tier, size, tiles)
	dummy("send"..tier, tiles)
	dummy("recv"..tier, tiles)
end

teleporter(1, "20MW", 250, 4)
teleporter(2, "30MW", 1000, 6)
