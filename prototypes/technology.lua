
data:extend({
	{
		type = "technology",
		name = "bulkteleport-tech1",
		icon = "__bulkteleport__/graphics/tech1.png",
		icon_size = 128,
		effects = {
			{ type = "unlock-recipe", recipe = "bulkteleport-send1" },
			{ type = "unlock-recipe", recipe = "bulkteleport-recv1" },
		},
		prerequisites = {
			"logistics-2",
			"circuit-network",
			"electric-energy-accumulators-1",
			"advanced-electronics",
			"concrete",
		},
		unit = {
			count = 300,
			ingredients = {
				{"science-pack-1", 1},
				{"science-pack-2", 1},
				{"science-pack-3", 1},
			},
			time = 30
		},
		order = "a",
	},
	{
		type = "technology",
		name = "bulkteleport-tech2",
		icon = "__bulkteleport__/graphics/tech1.png",
		icon_size = 128,
		effects = {
			{ type = "unlock-recipe", recipe = "bulkteleport-send2" },
			{ type = "unlock-recipe", recipe = "bulkteleport-recv2" },
		},
		prerequisites = {
			"bulkteleport-tech1",
			"advanced-electronics-2",
		},
		unit = {
			count = 400,
			ingredients = {
				{"science-pack-1", 1},
				{"science-pack-2", 1},
				{"science-pack-3", 1},
				{"high-tech-science-pack", 1},
			},
			time = 30
		},
		order = "b",
	},
})
