
local function serialize(t)
	local s = {}
	for k,v in pairs(t) do
		if type(v) == "table" then
			v = serialize(v)
		end
		s[#s+1] = tostring(k).." = "..tostring(v)
	end
	return "{ "..table.concat(s, ", ").." }"
end

local function queue()
	return {first = 0, last = -1}
end

local function qlen(list)
	return math.max(0, list.last - list.first + 1)
end

local function shove (list, value)
	local first = list.first - 1
	list.first = first
	list[first] = value
end

local function push (list, value)
	local last = list.last + 1
	list.last = last
	list[last] = value
end

local function shift (list)
	local first = list.first
	if first > list.last then return nil end
	local value = list[first]
	list[first] = nil
	list.first = first + 1
	return value
end

local function pop (list)
	local last = list.last
	if list.first > last then return nil end
	local value = list[last]
	list[last] = nil
	list.last = last - 1
	return value
end

local function prefixed(str, start)
	return str:sub(1, #start) == start
end

local function suffixed(str, ending)
	return ending == "" or str:sub(-#ending) == ending
end

local function distance(a, b)
	local x = b.x - a.x
	local y = b.y - a.y
	return math.sqrt(x*x + y*y)
end

local function chase_away_ghosts(surface, position)
	local ghosts = surface.find_entities_filtered({
		ghost_name = {
			"bulkteleport-energizer-send1",
			"bulkteleport-energizer-recv1",
			"bulkteleport-energizer-send2",
			"bulkteleport-energizer-recv2",
			"bulkteleport-buffer-send1",
			"bulkteleport-buffer-recv1",
			"bulkteleport-buffer-send2",
			"bulkteleport-buffer-recv2",
		},
		area = {
			{ position.x-1, position.y-1, },
			{ position.x+1, position.y+1, },
		},
	})
	for _, ghost in ipairs(ghosts) do
		ghost.destroy()
	end
end

local function OnEntityCreated(event)
	local entity = event.created_entity

	if prefixed(entity.name, "bulkteleport-send") then
		chase_away_ghosts(entity.surface, entity.position)

		local buffer = entity.surface.create_entity({
			name = "bulkteleport-buffer-send" .. entity.name:sub(-1),
			position = entity.position,
			force = entity.force,
		})

		local energizer = entity.surface.create_entity({
			name = "bulkteleport-energizer-send" .. entity.name:sub(-1),
			position = entity.position,
			force = entity.force,
		})

		global.entities[buffer.unit_number] = {
			entity = buffer,
			jobs = 0,
			energizer = energizer,
			networks = {},
		}

		push(global.queue, buffer.unit_number)
		entity.destroy()
		return
	end

	if prefixed(entity.name, "bulkteleport-recv") then
		chase_away_ghosts(entity.surface, entity.position)

		local buffer = entity.surface.create_entity({
			name = "bulkteleport-buffer-recv" .. entity.name:sub(-1),
			position = entity.position,
			force = entity.force,
		})

		local energizer = entity.surface.create_entity({
			name = "bulkteleport-energizer-recv" .. entity.name:sub(-1),
			position = entity.position,
			force = entity.force,
		})

		global.entities[buffer.unit_number] = {
			entity = buffer,
			jobs = 0,
			energizer = energizer,
			networks = {},
		}

		push(global.queue, buffer.unit_number)
		entity.destroy()
		return
	end

	-- blueprint
	if prefixed(entity.name, "bulkteleport-buffer-send") then
		chase_away_ghosts(entity.surface, entity.position)

		local buffer = entity

		local energizer = entity.surface.create_entity({
			name = "bulkteleport-energizer-send" .. entity.name:sub(-1),
			position = entity.position,
			force = entity.force,
		})

		global.entities[buffer.unit_number] = {
			entity = buffer,
			jobs = 0,
			energizer = energizer,
			networks = {},
		}

		push(global.queue, buffer.unit_number)
		return
	end

	-- blueprint
	if prefixed(entity.name, "bulkteleport-buffer-recv") then
		chase_away_ghosts(entity.surface, entity.position)

		local buffer = entity

		local energizer = entity.surface.create_entity({
			name = "bulkteleport-energizer-recv" .. entity.name:sub(-1),
			position = entity.position,
			force = entity.force,
		})

		global.entities[buffer.unit_number] = {
			entity = buffer,
			jobs = 0,
			energizer = energizer,
			networks = {},
		}

		push(global.queue, buffer.unit_number)
		return
	end
end

local function OnEntityRemoved(event)
	local entity = event.entity

	if prefixed(entity.name, "bulkteleport-buffer-send")
		or prefixed(entity.name, "bulkteleport-buffer-recv")
	then

		local id = entity.unit_number
		local state = global.entities[id]

		if state then

			for _, net in ipairs(state.networks) do
				global.networks[net][id] = nil
			end

			if state.energizer.valid then
				state.energizer.destroy()
			end

			global.entities[id] = nil
		end
	end
end

local function check_networks(state)
	local id = state.entity.unit_number

	for _, net in ipairs(state.networks) do
		global.networks[net][id] = nil
	end

	state.networks = {}
	state.green = nil
	state.red = nil

	local signals = state.entity.get_merged_signals()
	if signals ~= nil then
		for _, v in pairs(signals) do
			if v.signal.type == "virtual" and v.signal.name ~= nil then
				if v.signal.name == "signal-red" then
					state.red = true
				elseif v.signal.name == "signal-green" then
					state.green = true
				else
					local net = v.signal.name.."-"..v.count
					table.insert(state.networks, net)
					global.networks[net] = global.networks[net] or {}
					global.networks[net][id] = true
				end
			end
		end
	end
end

local function find_target(state)

	local id = state.entity.unit_number
	local targets = {}
	local name = "bulkteleport-buffer-recv" .. state.entity.name:sub(-1)

	local selected = nil

	for _, net in ipairs(state.networks) do
		for tid, _ in pairs(global.networks[net]) do
			local target = global.entities[tid]
			if target and target.entity.valid
				and target.entity.name == name
				and not target.energizer.is_crafting()
				and not target.shipment
				and not target.red
			then
				if not selected then
					selected = target
				else
					local sinventory = selected.entity.get_inventory(defines.inventory.chest)
					local tinventory = target.entity.get_inventory(defines.inventory.chest)
					if tinventory.get_item_count() < sinventory.get_item_count() then
						selected = target
					end
				end
			end
		end
	end

	return selected
end

local function notify(state, msg)

	if state.notify_msg == msg
		and state.notify_tick > game.tick - 60*5
	then
		return
	end

	state.notify_msg = msg
	state.notify_tick = game.tick

	state.entity.surface.create_entity({
		name = "flying-text",
		position = state.entity.position,
		color = {r=1,g=1,b=1},
		text = msg,
	})
end

local function warning(state, msg)

	if state.warning_msg == msg
		and state.warning_tick > game.tick - 60*5
	then
		return
	end

	state.warning_msg = msg
	state.warning_tick = game.tick

	state.entity.surface.create_entity({
		name = "flying-text",
		position = state.entity.position,
		color = {r=1,g=0.6,b=0.6},
		text = msg,
	})
end

local function OnTick(event)

	if game.tick % 55 == 0 then

		for i = 1, math.min(qlen(global.queue), 10), 1 do

			local id = shift(global.queue)
			local state = global.entities[id]

			if id and state and state.entity.valid then
				push(global.queue, id)

				if not state.energizer.is_crafting() then
					check_networks(state)

					if prefixed(state.entity.name, "bulkteleport-buffer-send") then
						local source = state
						local inventory = source.entity.get_inventory(defines.inventory.chest)

						-- shipment sent
						if source.energizer.products_finished > state.jobs then
							state.jobs = source.energizer.products_finished
						end

						local empty = inventory.is_empty()
						local full = not inventory.can_insert({ name = "iron-plate" })

						-- shipment ready to send
						if not source.red and not empty and (full or source.green) then
							local target = find_target(source)
							if target then

								local range = distance(source.entity.position, target.entity.position)
								local cost = math.min(10, math.ceil(range/1000))

								notify(source, string.format("%dm (%d)", range, cost))

								source.energizer.insert({ name = "bulkteleport-job-"..cost.."-"..source.entity.name:sub(-1) })
								target.energizer.insert({ name = "bulkteleport-job-"..cost.."-"..target.entity.name:sub(-1) })

								target.shipment = inventory.get_contents()
								inventory.clear()
							else
								warning(source, "No target...")
							end
						end
					end

					if prefixed(state.entity.name, "bulkteleport-buffer-recv") then

						local target = state
						local energizer = target.energizer
						local inventory = target.entity.get_inventory(defines.inventory.chest)

						-- shipment arrived; try to export
						if energizer.products_finished > target.jobs and target.shipment then
							local overflow = {}
							local retry = false
							for name, count in pairs(target.shipment) do
								local inserted = inventory.insert({
									name = name,
									count = count
								})
								if inserted < count then
									retry = true
									overflow[name] = count-inserted
								end
							end
							if retry then
								target.shipment = overflow
							else
								target.shipment = nil
								target.jobs = energizer.products_finished
							end
						end
					end
				end
			end
		end
	end
end

script.on_init(function()
	global.queue = global.queue or queue()
	global.entities = global.entities or {}
	global.networks = global.networks or {}

	script.on_event(defines.events.on_tick, OnTick)
	script.on_event({defines.events.on_built_entity, defines.events.on_robot_built_entity}, OnEntityCreated)
	script.on_event({defines.events.on_player_mined_entity, defines.events.on_robot_pre_mined, defines.events.on_entity_died}, OnEntityRemoved)
end)

script.on_load(function()
	script.on_event(defines.events.on_tick, OnTick)
	script.on_event({defines.events.on_built_entity, defines.events.on_robot_built_entity}, OnEntityCreated)
	script.on_event({defines.events.on_player_mined_entity, defines.events.on_robot_pre_mined, defines.events.on_entity_died}, OnEntityRemoved)
end)

